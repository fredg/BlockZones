#!/bin/bash
#set -x
[ -n "$TERM" ] && clear
###
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
#
# License: GNU/LGPL
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/04/22
#
###

###
#
# For OpenBSD
#
###

export LC_ALL=C

#IPv6=0
RACINE="$(dirname "$(readlink -f -- "$0")")"
DIR_DL="${RACINE}/downloads"
DIR_LISTS="${RACINE}/lists"
DIR_SRC="${RACINE}/src"

list="badips"

now="$(date +"%x %X")"
today="$(date -d "${now}" +%s)"

declare -i seconds=86400   # in seconds

declare -a blocklists

ARG="$1"
if [ -z "${ARG}" ]; then ARG="unbound"; fi

###
#
# Functions
#
###

### Get data into file in var list...
build_blocklists() {

    printf "%s \n" "*** Manage list: ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do

            if echo "${line}" | grep -v "^#"; then blocklists[$i]="${line}"; fi
            let i++

        done < "${DIR_SRC}/${list}"
        unset i

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "The file ${DIR_SRC}/${list} seems not to be readable!"
        exit 1

    fi

    }

build_sums() {

    if [ -f "${DIR_LISTS}/${output}" ];  then

        cd "${DIR_LISTS}" || exit 1

        if sha512sum --tag "${output}" > "${output}.sha512"; then
        
			printf "[ \\33[1;32m%s\\33[0;39m ] %s \n" "OK" "The checksum file is correctly created!"
        
        else
		
			printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem to create checksum file!"
        
        fi
        
        cd "${RACINE}" || exit 1

    fi

    }

download() {

    printf "%s \n" "########## Attempt to get blocklists files ##########"
    printf "%s \n" "=> Attempt to download file: ${filename}"

    local bool=0
    
    [ ! -d "${DIR_DL}" ] && mkdir "${DIR_DL}"

    if [ -x "$(which curl)" ]; then

        if ! "$(which curl)" -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -o "${filename}" "${url}"; then
            bool=1
        fi

    elif [ -x "$(which wget)" ]; then

        if ! "$(which wget)" --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -O "${filename}" "${url}"; then
            bool=1
        fi

    fi

    if [ ${bool} -eq 0 ]; then

        printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The file ${filename} is correctly downloaded!"

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem with download file ${filename}!"

    fi

    }

del_uniq_list() {

    if [ -f "${DIR_SRC}/${list}_ipv4" ]; then rm "${DIR_SRC}/${list}_ipv4"; fi
    if [ -f "${DIR_SRC}/${list}_ipv6" ]; then rm "${DIR_SRC}/${list}_ipv6"; fi

}

make_uniq_list() {

    if [ ! -f "${DIR_SRC}/${list}_ipv4" ]; then touch "${DIR_SRC}/${list}_ipv4"; fi
    if [ ! -f "${DIR_SRC}/${list}_ipv6" ]; then touch "${DIR_SRC}/${list}_ipv6"; fi

    #mime="text/plain"
    
    ##if [ "$(file -b -i "${filename}")" = "${mime}" ]; then

        #printf "%s \n" "====> Attempt to make uniq file with filename: ${filename}"

        #while read -r line; do

            ##if contains "${line}" "." ; then 
            #if valid_ipv4 "${line}"; then
				#echo "${line}" >> "${DIR_SRC}/${list}_ipv4"
			#fi
            
            ##if contains "${line}" ":" ; then 
            #if valid_ipv6 "${line}"; then
				#echo "${line}" >> "${DIR_SRC}/${list}_ipv6"
            #fi
            
        #done < "${filename}"

    #fi
    
    printf "%s \n" "====> Attempt to make uniq file with filename: ${filename}"
        
    printf "%s \n" "=> Check ipv4!"
    valid_ipv4 "${filename}" >> "${DIR_SRC}/${list}_ipv4"
        
    printf "%s \n" "=> Check ipv6!"
    valid_ipv6 "${filename}" >> "${DIR_SRC}/${list}_ipv6"
    
    # delete empties spaces and lines...
    for f in "${DIR_SRC}/${list}_ipv4" "${DIR_SRC}/${list}_ipv6"; do
		{ rm "${f}" && sed -e "/^$/d;/^[[:space:]]*$/d" | sort -du -o "${f}"; } < "${f}"
    done

    unset mime

    }

# Create uniq list file by datas into array blocklist
mng_blocklists() {

    printf "%s \n" "### Attempt to read datas into array blocklist"

    count="${#blocklists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${blocklists[@]}"; do

			ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"

            #printf "file: %s \n" "${file}"

            # define seconds before new dl
            case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac

            if [ -f "${filename}" ]; then

                # get file seconds stat
				if [ -x "$(which stat)" ]; then
					file_seconds=$(stat -c "%Y" "${filename}")
				else
					file_seconds=$(date -r "${filename}" +%s)
				fi

                # calcul diff time in seconds
                if [ -x "$(which bc)" ]; then
					diff_sec=$(echo "${today} - ${file_seconds}" | bc)
				else
					diff_sec=$((today - file_seconds))
				fi

                #unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then download; fi

            else

                download

            fi

            case "${ndd}" in
				"www.openbl.org")
					uncompress
					
					filename="${filename%.gz}"
				;;
            esac
			
			purge_files

            make_uniq_list

            unset filename

        done

    else

        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems not have datas!"
        exit 1

    fi

    unset count

    }

purge_files() {

    printf "%s \n" "===> Attempt to transform downloaded file: ${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one

    case "${ndd}" in
		"danger.rulez.sk")  # delete line ^#, and keep only adr ip
            #sed -i -e "/^#/d;s/^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*/\1/g;s/ \+//g" "${filename}"
            sed -i -e "/^#/d;s/\(.*\)#\(.*\)/\1/g" "${filename}"
        ;;
        "feeds.dshield.org")    # delete empty lines, and ^#
            #sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s/ \+//g" "${filename}"
            
            rm -f "${filename}_awk"
            
            while read -r line; do
				echo "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
            done < "${filename}"
            
            filename="${filename}_awk"
            #sed -i -e "s#\(.*\)\/$#\1#g" "${filename}"
            
        ;;
		"malc0de.com")  # delete empty lines, and ^//
            sed -i -e "/^$/d;/^\/\//d;s/ \+//g" "${filename}"
        ;;
        "myip.ms")  # delete empty lines, spaces, and keep only adr ip
            sed -i -e "/^$/d;/^#/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
        ;;
        "sslbl.abuse.ch")   # delete empty lines, ^#, ...
            #sed -i -e "/^$/d;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\,.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;/^#/d;s#\(.*\),\(.*\),\(.*\)#\1#g;s/ \+//g" "${filename}"
        ;;
		"www.spamhaus.org") # delete empty lines, ...
            sed -i -e "/^\;/d;s/ \+//g;s#\(.*\)\;SBL\(.*\)#\1#g" "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

    }

transformer() {
	
	for f in "${DIR_SRC}/${list}_ipv4" "${DIR_SRC}/${list}_ipv6"; do
	
		printf "%s \n" "===> Attempt to transform uniq list '${f}' to badips files"
		
		if [ -f "${f}" ]; then
		
			printf "[ \\33[1;32m%s\\33[0;39m ] %s \n" "OK" "The file '${f}' seems to be build!"
			
			output="$(basename "${f}")"
			
			mv "${f}" "${DIR_LISTS}/${output}"
			
			build_sums
			
			unset output
			
		else
		
			printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems no file: '${f}' !"
		
		fi
		
		
    done

    }

uncompress() {

    if [ "$(file -b -i "${filename}")" = "application/gzip" ]; then
        printf "%s \n" "==> Attempt to extract archive .gz: ${filename}"
        gunzip -d -f -q "${filename}";
    fi
    
    if [ "$(file -b -i "${filename}")" = "application/x-gzip" ]; then
        printf "%s \n" "==> Attempt to extract archive .gz: ${filename}"
        gunzip -d -f -q "${filename}";
    fi

    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
        printf "%s \n" "==> Attempt to extract archive .zip: ${filename}"
        unzip -oqu "${filename}" -d "${filename%.zip}"
    fi

    }

# functions valid_ip**() inspired 
# by: https://helloacm.com/how-to-valid-ipv6-addresses-using-bash-and-regex/
valid_ipv4() {
	
	egrep '^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/?([0-9]{1,2})?)$' "${1}"
	
	#if [[ "$1" =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]; then
		#return 0;
	#else
		#return 1;
	#fi
	
}

valid_ipv6() {
	
	egrep '^([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])$' "${1}"
	
	#if [[ "$1" =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
		#return 0;
	#else
		#return 1;
	#fi
	
}

del_uniq_list

build_blocklists

mng_blocklists

transformer
