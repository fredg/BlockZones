########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/27
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################
### Ty to @kuniyoshi for FreeBSD Codes - 2017/07/10
########################################################################

# Get languages texts
[ -f "${DIR_LANG}/texts.${lang}" ] && . "${DIR_LANG}/texts.${lang}" || . "${DIR_LANG}/texts.en" 
[ -f "${DIR_LANG}/titles.${lang}" ] && . "${DIR_LANG}/titles.${lang}" || . "${DIR_LANG}/titles.en" 

### Get data into file in array...
build_lists() {

	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${txt_mng_list} ${DIR_SRC}/${list} " >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "#" "*** ${txt_mng_list} ${DIR_SRC}/${list} ***"

    if [ -f "${DIR_SRC}/${list}" ]; then

        i=0
        while read -r line; do
        
            if [ "${verbose}" -eq 1 ]; then
				if echo "${line}" | grep -v "^#"; then lists[$i]="${line}"; fi
			
			else
				if echo "${line}" | grep -qv "^#"; then lists[$i]="${line}"; fi
				
			fi
            let i++
        
        done < "${DIR_SRC}/${list}"
        
        unset i
        
        [ "${debug}" -eq 1 ] && printf "%s \n" "${lists[@]}" >> "${bz_log}"

    else
		[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_file}${DIR_SRC}/${list}${txt_not_readable}" >> "${bz_log}"
        display_mssg "KO" "${txt_file}${DIR_SRC}/${list}${txt_not_readable}"
        
        byebye

    fi

}

build_uniq_list() {

    # on s'assure d'une liste de noms uniques
    ## http://promberger.info/linux/2009/01/14/removing-duplicate-lines-from-a-file/

    #{ rm "${DIR_SRC}/uniq_${list}" && awk '!x[tolower($1)]++' > "${DIR_SRC}/uniq_${list}"; } < "${DIR_SRC}/uniq_${list}"
    # shellcheck disable=SC2094
    { rm "${FILES[0]}" && awk '!x[tolower($1)]++' | sort -du -o "${FILES[0]}"; } < "${FILES[0]}"

}

### Create sha512 checksums files!
build_sums() {
	
	if [ "${one_checksum_file}" = 0 ]; then 
	
		typeset bool=1
	
		case "${list}" in
			"bogons") output="${file}" ;;
		esac

		if [ -f "${DIR_LISTS}/${output}" ];  then

			cd "${DIR_LISTS}" || exit 1
        
			case "${OSN}" in 
        
				"FreeBSD")
					if sha512 "${output}" > "${output}.sha512"; then bool=0; fi
				;;
        
				"OpenBSD")
					if sha512 -h "${output}.sha512" "${output}"; then bool=0; fi
				;;
			
			esac
        
			if [ ${bool} -eq 0 ]; then
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${title_checksums}" --infobox "${txt_dlg_ok}${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_checksum}'${DIR_LISTS}/${output}'${txt_created}"
				fi
			
				create_sign
        
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_create_checksum_file}${DIR_LISTS}/${output}" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${title_checksums}" --infobox "${txt_dlg_ko}${txt_error_create_checksum_file}${DIR_LISTS}/${output}" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_checksum_file}${DIR_LISTS}/${output}"
				fi
        
			fi
        
			cd "${ROOT}" || exit 1

		fi
    
		unset bool
		
	fi

}

########################################################################

byebye() {

	[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_stop} \n\n ${txt_search_reason}" >> "${bz_log}"
	
	if [ ${dialog} -eq 1 ]; then
		dialog --colors --backtitle "${ttl_project_name}" --title "${title_byebye}" --msgbox "${txt_dlg_ko}${txt_stop} \n\n ${txt_search_reason}" 10 100
		
	else
		display_mssg "KO" "${txt_stop}"
		display_mssg "KO" "${txt_search_reason}"
		
	fi
    
    exit 1;

}

########################################################################

check_needed_softs() {
	
	# curl 
	if [ -f /usr/local/bin/curl ]; then 
		use_curl=1
	
	else
		[ $cron -eq 0 ] && display_mssg "hg" "${txt_need_curl}"
		sleep 1
	
	fi
	
	# wget
	if [ -f /usr/local/bin/wget ]; then 
		use_wget=1
	
	else
		[ $cron -eq 0 ] && display_mssg "hg" "${txt_need_wget}"
		sleep 1
	
	fi
	
	# unzip
	if [ ! -f /usr/local/bin/unzip ]; then 
		display_mssg "KO" "${txt_error_unzip_tool}"
		display_mssg "#" "${txt_plz_install_tool}unzip"
		byebye
	fi
	
	# signify
	if [ "${OSN}" = "FreeBSD" ]; then
		if [ ! -f /usr/local/bin/signify ]; then 
			use_sign=0
			
			display_mssg "KO" "${txt_error_signify}"
			
			if confirm "${txt_answer_without_signify}"; then
				display_mssg "#" "${txt_without_signify}"
				
				sleep 1
			else
				display_mssg "#" "${txt_plz_install_tool}signify"
				byebye
			fi
		fi
	fi
	
}

########################################################################

confirm () {

    read -r response?"${1}${txt_yn}"
    case "$response" in
		# O is not zero: O(ui) ;)
        y|Y|o|O|1)	true ;;
        *)			false ;;
    esac

}

########################################################################

create_one_sums() {
	
	if [ "${one_checksum_file}" = 1 ]; then
	
		cd "${DIR_LISTS}" || exit 1
	
		find ./ -type f -exec sha512 {} + > "${DIR_SRC}/${NAME}.sha512"; 
	
		if [ -f "${DIR_SRC}/${NAME}.sha512" ]; then
	
			[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_created}" >> "${bz_log}"
			if [ ${dialog} -eq 1 ]; then
				dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${ttx_dlg_ok}${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_created}" 7 100
				sleep 1
			else
				display_mssg "OK" "${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_created}"
			fi
		
			if mv "${DIR_SRC}/${NAME}.sha512" "${DIR_LISTS}"; then
			
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_moved}${DIR_LISTS}" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}s" --infobox "${ttx_dlg_ok}${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_moved}${DIR_LISTS}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_checksum}'${DIR_SRC}/${NAME}.sha512'${txt_moved}${DIR_LISTS}"
				fi
			
				create_sign
			
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${DIR_SRC}/${NAME}.sha512'${txt_into}${DIR_LISTS}" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${txt_dlg_ko}${txt_error_move_file}'${DIR_SRC}/${NAME}.sha512'${txt_into}${DIR_LISTS}" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_move_file}'${DIR_SRC}/${NAME}.sha512'${txt_into}${DIR_LISTS}"
				fi
			
			fi
	
		else
			[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_create_checksum_file}'${DIR_SRC}/${NAME}.sha512'" >> "${bz_log}"
			if [ ${dialog} -eq 1 ]; then
				dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_checksum}" --infobox "${txt_dlg_ko}${txt_error_create_checksum_file}'${DIR_SRC}/${NAME}.sha512'" 7 100
				sleep 1
			else
				display_mssg "KO" "${txt_error_create_checksum_file}'${DIR_SRC}/${NAME}.sha512'"
			fi
			
		fi
	
		cd "${ROOT}" || exit 1

	fi
	
}

create_sign() {
	
	if [ "${use_sign}" = 1 ]; then
	
		if [ "${one_checksum_file}" = 1 ]; then 
			
			if signify -S -s "${dir_sec_key_signify}" -m "${DIR_LISTS}/${NAME}.sha512" -e -x "${DIR_LISTS}/${NAME}.sha512.sig"; then
				
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file_sign}'${DIR_LISTS}/${NAME}.sha512.sig'${txt_created}" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ok}${txt_file_sign}'${DIR_LISTS}/${NAME}.sha512.sig'${txt_created}" 7 100
					sleep 1
					
				else
					display_mssg "OK" "${txt_file_sign}'${DIR_LISTS}/${NAME}.sha512.sig'${txt_created}"
				fi
	
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/${NAME}.sha512.sign" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ko}${txt_error_create_sign_file}'${DIR_LISTS}/${NAME}.sha512.sign'" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/${NAME}.sha512.sign'"
				fi
		
			fi
		
		else
			if signify -S -s "${dir_sec_key_signify}" -m "${DIR_LISTS}/${output}.sha512" -e -x "${DIR_LISTS}/${output}.sha512.sig"; then
				
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file_sign}'${DIR_LISTS}/${output}.sha512.sig'${txt_created}'" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ok}${txt_file_sign}'${DIR_LISTS}/${output}.sha512.sig'${txt_created}" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file_sign}'${DIR_LISTS}/${output}.sha512.sig'${txt_created}"
				fi
	
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/${output}.sha512.sign'" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_create_sign}" --infobox "${txt_dlg_ko}${txt_error_create_sign_file}'${DIR_LISTS}/${output}.sha512.sign'" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_create_sign_file}'${DIR_LISTS}/${output}.sha512.sign'"
				fi
		
			fi
		
		fi
	
	fi
	
}

########################################################################
### To delete files
del_files() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${ttl_del_files} " >> "${bz_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_del_files}"
	
	for f in "${FILES[@]}"; do
	
		[ "${verbose}" -eq 1 ] && echo "${txt_file}${f}"
		
		if [ -f "${f}" ]; then
		
			if rm "${f}"; then
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}${f}${txt_deleted}" >> "${bz_log}"
				[ "${verbose}" -eq 1 ] && display_mssg "OK" "${txt_file}${f}${txt_deleted}"
				
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_del_file}${f}" >> "${bz_log}"
				display_mssg "KO" "${txt_error_del_file}${f}"
				
			fi
		
		fi
		
	done
	
	del_sums
	
}

### To delete spaces, lines into files
del_spaces() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "* ${ttl_del_spaces}" >> "${bz_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "hb" "${ttl_del_spaces}"

	# delete empties spaces and lines...
    for f in "${FILES[@]}"; do
    
		{ rm "${f}" && sed -e "/^#/d;/^$/d;/^[[:space:]]*$/d" | sort -d -u -o "${f}"; } < "${f}"
    
    done
    
}

del_sums() {
	
	if [ "${one_checksum_file}" = 1 ]; then
	
		find "${DIR_LISTS}/" -type f -name "*\.sha512*" -exec rm -f {} +
	
	else
		rm -f "${DIR_LISTS}/${NAME}.sha512*"
	
	fi
	
}

########################################################################

display_mssg() {

    typeset statut info 
    statut="$1" info="$2" 
    
    case "${statut}" in
        "KO"|1)	color="${red}" 		;;
        "OK"|0)	color="${green}" 	;;
    esac

	if [ "${statut}" == "#" ]; then
		if [ "${OSN}" = "FreeBSD" ] && [ $use_color -eq 1 ]; then
			printf "%s" "[ "; tput bold; printf "%s" "${info}"; tput sgr0; printf "%s" "] \n";
			
		else
			printf "[ ${bold}%s${neutral} ] \n" "${info}"
			
		fi
	
	elif [ "${statut}" == "hb" ]; then
		if [ "${OSN}" = "FreeBSD" ] && [ $use_color -eq 1 ]; then
			tput dim; printf "%s \n" "${info}"; tput sgr0; printf "\n"
			
		else
			printf "${dim}%s${neutral} \n" "${info}"
			
		fi
		
	else
		if [ "${OSN}" = "FreeBSD" ] && [ $use_color -eq 1 ]; then
			case "${statut}" in
				"KO")
					printf "%s" "[ "; tput setaf 1; printf "%s" "KO"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
				"OK")
					printf "%s" "[ "; tput setaf 2; printf "%s" "OK"; tput sgr0; printf "%s \n" " ] ${info}"
				;;
			esac
			
		else
			printf "[ ${color}%s${neutral} ] %s \n" "${statut}" "${info}"
			
		fi
	
	fi

    unset info statut text

}

display_mssg_end() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${txt_goodbye} :: ${txt_ended} ###" >> "${bz_log}"
	
	if [ ${dialog} -eq 1 ]; then 
	
		dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_project_name}" --msgbox "${txt_dlg_goodbye}" 10 100
		
		dialog --clear
		clear
	
	else
		
		[ ${cron} -eq 0 ] && echo "
########################################################################
####
## 
#   ${txt_goodbye}";

		echo "#	${txt_ended} ";

		[ ${cron} -eq 0 ] && echo "#   ${txt_hope} 
##
###
########################################################################
"
	
	fi
	
	exit 0
	
}

display_welcome() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${txt_welcome} :: ${txt_execute}$0${txt_obtain}${list} ###" >> "${bz_log}"
	
	if [ ${dialog} -eq 1 ]; then 

		if dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_project_name}" --yesno "${txt_dlg_welcome}" 10 100; then
			sleep 1
		
		else
			display_mssg_end
			
		fi

	else
		
		[ ${cron} -eq 0 ] && echo "
########################################################################
####
## ";

		echo "#   ${txt_welcome} ";
		
		[ ${cron} -eq 0 ] && echo "##
###
## ";

		echo "#	${txt_execute}$0${txt_obtain}${list} ";

		[ ${cron} -eq 0 ] && echo "##
###
########################################################################
"
	fi

}

########################################################################
### Downloader needed files
download() {

	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${ttl_download}: ${txt_download_file}${url}" >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_download}"
    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_download_file}${url}"

    typeset bool=0 options=""
    
    [ ! -d "${DIR_DL}" ] && mkdir "${DIR_DL}"

    if [ "${use_curl}" = 1 ]; then
    
		if [ ${dialog} -eq 1 ]; then options="-s "; fi
		
		if [ ${verbose} -eq 1 ]; then options="-v "; fi
		
		if [ ${debug} -eq 1 ]; then 
			options='-v -w "%{response_code};%{time_total}" '
			/usr/local/bin/curl -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" --compressed ${options} -o "${filename}" "${url}" 2>&1 | tee >> "${bz_log}"
		
		elif ! /usr/local/bin/curl -A "Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" --compressed ${options} -o "${filename}" "${url}"; then
			bool=1
		fi

    elif [ "$(use wget)" = 1 ]; then

		if [ ${dialog} -eq 1 ]; then options+="-q "; fi
					
		if [ ${debug} -eq 1 ]; then 
			options+="-d "
			/usr/local/bin/wget --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -c "${options}" -O "${filename}" "${url}" 2>&1 | tee >> "${bz_log}"
		
		elif ! /usr/local/bin/wget --user-agent="Mozilla/5.0 (X11; Debian; Linux; rv:0.0) Gecko/20100101 Firefox/0.0" -c "${options}" -O "${filename}" "${url}"; then
			bool=1
		fi

    else
    
		options="-C -n -m"
    
		if [ ${dialog} -eq 1 ]; then options="-C -n -V"; fi

		if [ ${debug} -eq 1 ]; then 
			options="-C -d -n -m "
			/usr/bin/ftp "${options}" -o "${filename}" "${url}" 2>&1 | tee >> "${bz_log}"
			
		elif ! /usr/bin/ftp "${options}" -o "${filename}" "${url}"; then 
			bool=1; 
			
		fi

    fi

    if [ ${bool} -eq 0 ]; then

        [ "${verbose}" -eq 1 ] && display_mssg "OK" "${txt_file}${filename}${txt_downloaded}"

    else

        display_mssg "KO" "${txt_error_download_file}${filename}!"
        byebye

    fi
    
    unset bool

}

########################################################################

install_service() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${txt_installer}" >> "${bz_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${txt_installer}"
	
	if [ "${install_list}" ]; then
		
		case "${choice}" in 
	
			"blacklists") cp_for_blacklists ;;
			
		esac
		
	fi
	
}

cp_for_blacklists() {

	dir_dest=""

	case "${choice_bl}" in
	
		"bind"|"bind8"|"bind9") dir_dest="${dir_dest_bind}"	;;
		
		"host"|"hosts"|"host0") dir_dest="${dir_dest_host}" ;;
	
		"unbound") dir_dest="${dir_dest_unbound}" ;;
	
	esac
	
	if [ -n "${dir_dest}" ]; then
	
		# if file exists, mv to file_dated (for backup)
		if [ -f "${dir_dest}/${output}"; then
			
			if [ "${use_timestamp}" ]; then
				if mv "${dir_dest}/${output}" "${dir_dest}/${output}.bckp_${timestamp}"; then
				
					[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${timestamp}'" >> "${bz_log}"
					if [ ${dialog} -eq 1 ]; then
						dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${ttx_dlg_ok}${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${timestamp}'" 7 100
						sleep 1
					else
						display_mssg "OK" "${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${timestamp}'"
					fi
				
				else
					[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${timestamp}'" >> "${bz_log}"
					if [ ${dialog} -eq 1 ]; then
						dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${txt_dlg_ko}${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${timestamp}'" 7 100
						sleep 1
					else
						display_mssg "KO" "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${timestamp}'"
					fi
				
				fi
			
			else
				if mv "${dir_dest}/${output}" "${dir_dest}/${output}.bckp_${today}"; then 
					[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${today}'" >> "${bz_log}"
					if [ ${dialog} -eq 1 ]; then
						dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${ttx_dlg_ok}${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${today}'" 7 100
						sleep 1
					else
						display_mssg "OK" "${txt_file}'${dir_dest}/${output}'${txt_moved}'${dir_dest}/${output}.bckp_${today}'"
					fi
					
				else
					[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${today}'" >> "${bz_log}"
					if [ ${dialog} -eq 1 ]; then
						dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${txt_dlg_ko}${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${today}'" 7 100
						sleep 1
					else
						display_mssg "KO" "${txt_error_move_file}'${dir_dest}/${output}'${txt_into}'${dir_dest}/${output}.bckp_${today}'"
					fi
				
				fi
			
			fi
			
		fi
		
		# cp new file to dest dir service
		if [ -f "${DIR_LISTS}/${output}" ]; then
			if cp "${DIR_LISTS}/${output}" "${dir_dest}"; then
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_moved}'${dir_dest}'" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${ttx_dlg_ok}${txt_file}'${DIR_LISTS}/${output}'${txt_moved}'${dir_dest}'" 7 100
					sleep 1
				else
					display_mssg "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_moved}'${dir_dest}'"
				fi
				
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'${txt_into}'${dir_dest}/'" >> "${bz_log}"
				if [ ${dialog} -eq 1 ]; then
					dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_installer}" --infobox "${txt_dlg_ko}${txt_error_move_file}'${DIR_LISTS}/${output}'${txt_into}'${dir_dest}/'" 7 100
					sleep 1
				else
					display_mssg "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'${txt_into}'${dir_dest}/'"
				fi
			
			fi
			
		fi
	
	fi

}

########################################################################

make_uniq_list() {
    
    touch_files

	[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_make_single_file}${filename}" >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_make_single_file}${filename}"
    
    case "${list}" in
		"badips")
		
			[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_check_ipv4}: ${filename} > ${FILES[0]}" >> "${bz_log}"
			[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv4}"
			valid_ipv4 "${filename}" >> "${FILES[0]}"
        
			[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_check_ipv6}: ${filename} > ${FILES[1]}" >> "${bz_log}"
			[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv6}"
			valid_ipv6 "${filename}" >> "${FILES[1]}"
		
		;;
		"bogons")
		
			case "${url}" in
				*"bogons-ipv4"*)
		
					[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_check_ipv4}: ${filename} > ${FILES[0]}" >> "${bz_log}"
					[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv4}"
					valid_ipv4 "${filename}" >> "${FILES[0]}"
		
				;;
				*"bogons-ipv6"*)
    
					[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_check_ipv6}: ${filename} > ${FILES[1]}" >> "${bz_log}"
					[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_check_ipv6}"
					valid_ipv6 "${filename}" >> "${FILES[1]}"
	
				;;
		
			esac
			
		;;
		"domains")
			
			[ "${debug}" -eq 1 ] && printf "%s \n" "* domains: tolower ${filename} > ${FILES[0]}" >> "${bz_log}"
			awk '{ print tolower($0) }' "${filename}" >> "${FILES[0]}"
			
		;;
	esac

	del_spaces

}

########################################################################

get_array_menu() {
	
	case "${choice}" in 
	
		"blacklists")
			set -A NAMES -- "${menus_blacklists[@]}"
		;;
	
		*)
			set -A NAMES -- "${menus[@]}"
		;;
		
	esac
	
	typeset -i nb="${#NAMES[@]}" i=0 j=0
	
	while (( i < $nb )); do
	
		n="${NAMES[$i]}"
		
		if [ "${choice}" == "blacklists" ]; then 
			text="txt_menus_bl_$n"
		else
			text="txt_menus_$n"
		fi
		
		#[[ "$VKSH" == *"PD KSH"* ]] && eval "ref=\${$text}" || typeset -n ref="${text}"
		eval "ref=\${$text}"
		
		dialog_menu[$j]="${NAMES[$i]}"
		dialog_menu[$j+1]="${ref}"
		
		let j+=2
		let i+=1
		
		unset ref text n
		
	done
	
}

menu() {
	
	display_welcome
	
	PS3="${txt_menu_ps3}"
	select option in "${menus[@]}"; do
		choice="$option"; break
	done
	
	echo "
${txt_choice}${choice}
	"
	
	case "${choice}" in
		
		"blacklists") 
	
			PS3="${txt_menu_ps3_bl}"
			select option in "${menus_blacklists[@]}"; do
				choice_bl="${option}"; break
			done
		
			echo "
${txt_choice_bl}${choice_bl}	
			"
		;;
		
		"quit") display_mssg_end ;;
	
	esac
	
	sleep 1
	
	setVariables
	
}

menu_dialog() {
	
	if [ -z "$1" ]; then
		set -A dialog_menu
		INPUT=/tmp/BZ_menu.sh.$$
	
		get_array_menu
	
		dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu}" --menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"

		return=$?
		choice="$(cat "${INPUT}" | tr [:upper:] [:lower:])"
		[ "${debug}" -eq 1 ] && printf "%s \n" "*** menu: $choice" >> "${bz_log}"
	
		[ -f "$INPUT" ] && rm -f "$INPUT"
		unset dialog_menu INPUT
	
		case "${return}" in
			1) display_mssg_end ;;
		esac
	
		if [ -z "${choice}" ]; then menu_dialog; fi
	else 
		choice="$1"
	fi
	
	case "${choice}" in
		
		"blacklists")
			
			set -A dialog_menu
			INPUT=/tmp/BZ_BL_menu.sh.$$
			
			get_array_menu
			
			dialog --clear --backtitle "${ttl_project_name}" --title "${ttl_menu_blacklist}" --menu "${txt_dialog_menu}" 0 0 10 "${dialog_menu[@]}" 2> "${INPUT}"
			
			return=$?
			choice_bl="$(cat "${INPUT}" | tr [:upper:] [:lower:])"
			[ "${debug}" -eq 1 ] && printf "%s \n" "*** menu: $choice | choice_bl: $choice_bl" >> "${bz_log}"
			
			case "${return}" in
				1) 
					if dialog --backtitle "${ttl_project_name}" --clear --title "${ttl_menu_blacklist}" --yesno "${txt_answer_sure}" 10 100; then
						display_mssg_end
					else
						menu_dialog "${choice}"
					fi
				;;
			esac
			
			[ -f "$INPUT" ] && rm -f "$INPUT"
			unset dialog_menu INPUT
		;;
		
		"quit") display_mssg_end ;;
		
	esac
	
	setVariables
	
}

########################################################################

mng_lists() {

	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${txt_read_array_bl}" >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "#" "${txt_read_array_bl}"
    
    case "$choice" in
    
		"badips")		[ "${dialog}" -eq 1 ] && mng_lists_bi4d || mng_lists_bi ;;
		"blacklists")	[ "${dialog}" -eq 1 ] && mng_lists_bl4d || mng_lists_bl ;;
		"bogons") 		[ "${dialog}" -eq 1 ] && mng_lists_bg4d || mng_lists_bg ;;
    
    esac

}

########################################################################

purge_files() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_purge_files}${filename}" >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_purge_files}${filename}"

    # /^$/d     <= empty line
    # /^#/d     <= line starting with #
    # /^\s*$/d  <= remove spaces'n tabs; /^[[:space:]]*$/d
    # s/ \+//g  <= remove all spaces
    # s/ \+/ /g  <= replace more spaces by only one
    
    case "$choice" in
    
		"badips")		purge_files_bi ;;
		"blacklists")	purge_files_bl ;;
		"bogons") 		purge_files_bg ;;
    
    esac
    
}

purge_files_bg() {

    [[ "${url}" != *"bz_bogons"* ]] && sed -i -e "/^$/d;/^#/d;/^[[:space:]]*$/d;" "${filename}"
    
    case "${filename}" in
		*"fullbogons-ipv4"*)
			sed -i -e "s/192\.168\(.*\)/#192.168\1/" "${filename}"
		;;
		#"fullbogons-ipv6.txt")
			#sed -i -e "" "${filename}"
		#;;
    esac
	
}

purge_files_bi() {

    case "${ndd}" in
		"danger.rulez.sk")  # delete line ^#, and keep only adr ip
            #sed -i -e "/^#/d;s/^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*/\1/g;s/ \+//g" "${filename}"
            sed -i -e "/^#/d;s/\(.*\)#\(.*\)/\1/g" "${filename}"
        ;;
        "feeds.dshield.org")    # delete empty lines, and ^#
            #sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\t.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;s#^Start\(.*\)#\#Start\1#g;/^#/d;s/ \+//g" "${filename}"
            
            rm -f "${filename}_awk"
            
            while read -r line; do
				echo "${line}" | awk '{print $1"/"$3}' >> "${filename}_awk"
            done < "${filename}"
            
            filename="${filename}_awk"
            #sed -i -e "s#\(.*\)\/$#\1#g" "${filename}"
            
        ;;
		"malc0de.com")  # delete empty lines, and ^//
            sed -i -e "/^$/d;/^\/\//d;s/ \+//g" "${filename}"
        ;;
        "myip.ms")  # delete empty lines, spaces, and keep only adr ip
            sed -i -e "/^$/d;/^#/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
        ;;
        "sslbl.abuse.ch")   # delete empty lines, ^#, ...
            #sed -i -e "/^$/d;/^#/d;s#^\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\,.*#\1#g;s/ \+//g" "${filename}"
            sed -i -e "/^$/d;/^#/d;s#\(.*\),\(.*\),\(.*\)#\1#g;s/ \+//g" "${filename}"
        ;;
		"www.spamhaus.org") # delete empty lines, ...
            sed -i -e "/^\;/d;s/ \+//g;s#\(.*\) \; SBL\(.*\)#\1#g" "${filename}"
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

}

purge_files_bl() {

    case "${ndd}" in
        "adaway.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;s/\(.*\) ##\(.*\)#/\1/g;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\ \(.*\)#\1#g;" "${filename}"
                ;;
            esac
        ;;
        "hosts-file.net")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^$/d;/^#/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1\t\(.*\)#\1#g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "mirror1.malwaredomains.com")
            case "${file}" in
                "immortal_domains.zip")
                    sed -i -e "/^#/d;/^notice/d;s/ \+//g" "${filename}"
                ;;
                "malwaredomains.zones.zip")
                    #sed -i -e "/^\/\//d;s/ \+/ /g;s#zone \"\(.*\)\" {type master; file \"/etc/namedb/blockeddomain.hosts\";};#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^\/\//d;s/ \+/ /g" | awk '{print $2}' | sed -e "s/\"//g" | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "someonewhocares.org")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    sed -i -e "/^#/d;/^\t\+#/d;s/^[ \t]*//g;/^ \+#/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d" "${filename}"
                ;;
                *)
                    #sed -i -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;/^$/d;s#127.0.0.1 \(.*\)#\1#g" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)local\(.*\)/d;/\(.*\)broadcast\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        "winhelp2002.mvps.org")
            case "${ARG}" in
                "host0")
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
                *)
                    sed -i -e "/^#/d;/^$/d;/^\r/d;/\(.*\)localhost\(.*\)/d;s#0.0.0.0 \(.*\)#\1#g;s/\(.*\)#\(.*\)/\1/g;s/ \+//g" "${filename}"
                ;;
            esac
        ;;
        "www.malwaredomainlist.com")
            case "${ARG}" in
                "host"|"hosts"|"host0")
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" "${filename}"
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
                *)
                    #sed -i -e "/^$/d;/^\r/d;/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d;s#127.0.0.1  \(.*\)#\1#g" "${filename}" #;
                    { rm "${filename}" && sed -e "/^#/d;/^[[:space:]]*$/d;/\(.*\)localhost\(.*\)/d" | awk '{print $2}' | sort -u -o "${filename}"; } < "${filename}"
                ;;
            esac
        ;;
        *)  # delete empty lines, ^#, keep all others :p
            sed -i -e "/^$/d;/^#/d;s/ \+//g" "${filename}"
        ;;
    esac

}

########################################################################

setLogs() {
	
	if [ ${debug} -eq 1 ]; then 
		
		if [ ! -d "${DIR_LOG}" ]; then mkdir "${DIR_LOG}"; fi
	
		mode="normal"
		[ "${cron}" -eq 1 ] && mode="cron"
		[ "${dialog}" -eq 1 ] && mode="dialog"
		[ "${verbose}" -eq 1 ] && mode="verbose"
	
		case "${use_timestamp}" in
			0) bz_log="${DIR_LOG}/${today}.log" ;;
			1) bz_log="${DIR_LOG}/${timestamp}.log"
		esac
		
		touch "${bz_log}"
		echo "##############################
### BlockZones :: File log
### Mode: ${mode}
###	Date: ${now}
### Script: $0
##############################
# color: ${use_color}
# on checksum file: ${one_checksum_file}
# use signify: ${use_sign}
##############################
" >> "${bz_log}"

		unset mode
		
	fi
	
}

########################################################################

setVariables() {
	
	case "$0" in

		*"badips"*) 	setVarsBadips ;;
	
		*"blacklists"*) setVarsBlacklists ;;
	
		*"bogons"*) 	setVarsBogons ;;
	
		*"menu"*)
	
			case "$choice" in
			
				"badips") 	setVarsBadips ;;
				"blacklists") 	setVarsBlacklists ;;
				"bogons")	setVarsBogons ;;
			
			esac
	
		;;
	
	esac
	
	[ "${verbose}" -eq 1 ] && printf "%s \n"  "list: $list | choice: $choice"
	
}

setVarsBadips() {
	
	list="badips"

	FILES[0]="${DIR_SRC}/${list}_ipv4"
	FILES[1]="${DIR_SRC}/${list}_ipv6"
		
	[ -z "$choice" ] && choice="${list}"
	
	if [ "${debug}" -eq 1 ]; then
		printf "%s \n" "*** setVariables: list: $list | choice: $choice" >> "${bz_log}"
		printf "%s \n%s\n" "* files:" "${FILES[@]}" >> "${bz_log}"
	fi
	
}

setVarsBlacklists() {
	
	list="domains"

	# if menu run.
	#if [ "${choice}" == "blacklists" ]; then
		#if [[ ${menus_blacklist[@]} != *"${choice_bl}"* ]]; then choice_bl="unbound"; fi
		##if [ -z "${ARG}" ]; then choice_bl="unbound"; fi
	#fi
		
	[ -z "$choice" ] && choice="blacklists"
	
	FILES[0]="${DIR_SRC}/${list}_${choice}"
	
	if [ "${debug}" -eq 1 ]; then
		printf "%s \n" "### setVariables: list: $list | choice: $choice | choice_bl: $choice_bl" >> "${bz_log}"
		printf "%s \n%s\n" "### files:" "${FILES[@]}" >> "${bz_log}"
		printf "%s \n" "### IPv4: ${IPv4} | IPv6: ${IPv6} " >> "${bz_log}"
	fi
	
}

setVarsBogons() {
	
	list="bogons"

	FILES[0]="${DIR_SRC}/${list}_ipv4"
	FILES[1]="${DIR_SRC}/${list}_ipv6"
		
	[ -z "$choice" ] && choice="${list}"
	
	if [ "${debug}" -eq 1 ]; then
		printf "%s \n" "*** setVariables: list: $list | choice: $choice" >> "${bz_log}"
		printf "%s \n%s\n" "* files:" "${FILES[@]}" >> "${bz_log}"
	fi
	
}

########################################################################
### To created files
touch_files() {
	
	for f in "${FILES[@]}"; do
		
		if [ ! -f "${f}" ]; then
		
			[ "${debug}" -eq 1 ] && printf "%s \n" "* ${ttl_touch_files}" >> "${bz_log}"
			[ "${verbose}" -eq 1 ] && display_mssg "#" "${ttl_touch_files}"
		
			if touch "${f}"; then
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}${f}${txt_created}" >> "${bz_log}"
				display_mssg "OK" "${txt_file}${f}${txt_created}"
				
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_create_file}${f}" >> "${bz_log}"
				display_mssg "KO" "${txt_error_create_file}${f}"
				
			fi
		
		fi
		
	done
	
}

########################################################################

transformer() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "*** ${txt_transform}" >> "${bz_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "#" "${txt_transform}"

	case "$choice" in
		
		"badips") 		[ "${dialog}" -eq 1 ] && transformer_bi4d || transformer_files ;;
		
		"blacklists")	[ "${dialog}" -eq 1 ] && transformer_bl4d || transformer_bl ;;
		
		"bogons") 		transformer_files ;;
			
		
	esac

}

transformer_files() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${ttl_transformer}: \n\n ${txt_wait_minutes}" >> "${bz_log}"
	[ "${verbose}" -eq 1 ] && display_mssg "hb" "${ttl_transformer}: \n\n ${txt_wait_minutes}"
	
	for f in "${FILES[@]}"; do
	
		[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_transform_file}'${f}" >> "${bz_log}"
		[ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_transform_file}'${f}'"
		
		if [ -f "${f}" ]; then
			
			output="$(basename "${f}")"
			
			if mv "${f}" "${DIR_LISTS}/${output}"; then 
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" >> "${bz_log}"
				display_mssg "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
				
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'" >> "${bz_log}"
				display_mssg "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'"
			
			fi
			
			build_sums
			
			unset output
			
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_file}${f}" >> "${bz_log}"
				display_mssg "KO" "${txt_error_no_file}'${f}'"
		
			fi
		
	done
	
}

########################################################################
### extract archive
uncompress() {

    if [ "$(file -b -i "${filename}")" = "application/gzip" ]; then
		[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_xtract_gz}${filename}" >> "${bz_log}"
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_gz}${filename}"
        /usr/bin/gunzip -d -f -q "${filename}";
    fi
    
    if [ "$(file -b -i "${filename}")" = "application/x-gzip" ]; then
		[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_xtract_gz}${filename}" >> "${bz_log}"
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_gz}${filename}"
        /usr/bin/gunzip -d -f -q "${filename}";
    fi

    if [ "$(file -b -i "${filename}")" = "application/zip" ]; then
		[ "${debug}" -eq 1 ] && printf "%s \n" "* ${txt_xtract_zip}${filename}" >> "${bz_log}"
        [ "${verbose}" -eq 1 ] && display_mssg "hb" "${txt_xtract_zip}${filename}"
        /usr/local/bin/unzip -oqu "${filename}" -d "${filename%.zip}"
    fi

}

########################################################################
# functions valid_ip**() inspired 
# by: https://helloacm.com/how-to-valid-ipv6-addresses-using-bash-and-regex/
valid_ipv4() {
	
	egrep '^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\/?([0-9]{1,2})?)$' "${1}"
	
}
	
valid_ipv6() {

	egrep '^([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])$' "${1}"
	
}

########################################################################

setLogs
[[ "$0" != *"menu"* ]] && setVariables
