########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
# Date: 2017/08/08
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bg4d() {

	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${ttl_manager}: ${txt_read_array_bl}" >> "${bz_log}"
    dialog --backtitle "${ttl_project_name}" --title "${ttl_manager}" --infobox "${txt_read_array_bl}" 10 100

    count="${#lists[@]}"
    modulo=$(echo "100/$count"|bc)
    
    [ "${debug}" -eq 1 ] && printf "%s %s | %s \n" "# manager:" "count: $count" "modulo: $modulo" >> "${bz_log}"

    if [ "${count}" -gt 0 ]; then
    
		(

        for url in "${lists[@]}"; do

			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			filename="${DIR_DL}/${file}"
			
			if [ "${debug}" -eq 1 ]; then
				printf "file: %s \n" "${file}" >> "${bz_log}"
				printf "filename: %s \n" "${filename}" >> "${bz_log}"
			fi
			
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $file
XXX
EOF

			[ "${debug}" -eq 1 ] && printf "nb seconds: %s \n" "${seconds}" >> "${bz_log}"

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    download

                fi

            else

                download

            fi
            
            if [ -f "${filename}" ]; then
            
				[ "${debug}" -eq 1 ] && printf "%s \n" "# ${ttl_manager}: ${txt_wait_download_file}${filename}" >> "${bz_log}"
				dialog --backtitle "${ttl_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_wait_download_file}${filename}" 10 100
				
				purge_files
				
				make_uniq_list
				
				if [ -f "${DIR_LISTS}/${file}" ]; then
				
					#[ "${verbose}" -eq 1 ] && display_mssg "OK" "The file '${DIR_LISTS}/${file}' seems to be build!"
					
					build_sums
				
				else
					[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error}${txt_file}'${DIR_LISTS}/${file}'${txt_error_not_build}" >> "${bz_log}"
					dialog --backtitle "${ttl_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error}${txt_file}'${DIR_LISTS}/${file}'${txt_error_not_build}" 10 100
				
				fi
				
            fi 

            unset filename

        done
        
        ) | dialog --backtitle "${ttl_project_name}" --title "${ttl_manager}" --gauge "${txt_wait}" 10 100 

    else
		[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_data}" >> "${bz_log}"
        dialog --backtitle "${ttl_project_name}" --colors --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error_no_data}" 7 100
        sleep 1
        
        byebye

    fi

    unset count

}
