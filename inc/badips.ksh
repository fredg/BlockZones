########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/08
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bi() {

    count="${#lists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${lists[@]}"; do

			ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"

            if [ "${debug}" -eq 1 ]; then
				printf "domain name: %s \n" "${ndd}" >> "${bz_log}"
				printf "file: %s \n" "${file}" >> "${bz_log}"
				printf "filename: %s \n" "${filename}" >> "${bz_log}"
			fi

            # define seconds before new dl
            case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
                "www.spamhaus.org") seconds=3600;; # 1 hours
                #*) seconds=86400;;
            esac
			
			[ "${debug}" -eq 1 ] && printf "nb seconds: %s \n" "${seconds}" >> "${bz_log}"
			
            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${timestamp} - ${file_seconds}" | bc)

                #unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then download; fi

            else

                download

            fi

            case "${ndd}" in
				"www.openbl.org")
					uncompress
					
					filename="${filename%.gz}"
				;;
            esac
			
			purge_files

            make_uniq_list

            unset filename

        done

    else
		[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_data}" >> "${bz_log}"
        display_mssg "KO" "${txt_error_no_data}"
        
        byebye

    fi

    unset count

}
